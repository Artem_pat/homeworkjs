//1) SetTimeout() запускає функцію один раз з затримкою яку ми задамо, setInterval() запускає функцію безліч разів з інтервалом який ми задамо.
//2) Спрацює відразу після завершення коду.
//3) Для setInterval функція залишається в пам’яті до виклику clearInterval.Функція посилається на зовнішнє лексичне середовище, тому, поки вона живе, 
//   зовнішні змінні також живуть. Вони можуть зайняти набагато більше пам’яті, ніж сама функція. Тому, коли нам більше не потрібна запланована функція, краще її скасувати, навіть якщо вона дуже мала.

const imgs = document.querySelectorAll('img');
const btnStop = document.querySelector('.stop');
const btnCont = document.querySelector('.continue');

let currentIndex = 0;
let imgInterval;

function showImages(index) {
    imgs.forEach((el, i) => {
        el.style.display = i === index ? 'block' : 'none'})
}

function stopShowImages() {
    clearInterval(imgInterval);
}

function startShowImages() {
    
    imgInterval = setInterval(() => {
       
        currentIndex = (currentIndex + 1) % imgs.length
        showImages(currentIndex);
        
    }, 3000)
}

setTimeout(function () {
    btnStop.style.display = 'block';
    btnCont.style.display = 'block';
},3000)

btnCont.addEventListener('click', startShowImages);

btnStop.addEventListener('click', stopShowImages);

showImages(currentIndex);
startShowImages();





// document.addEventListener("DOMContentLoaded", function () {
//     const images = document.querySelectorAll(".image-to-show");
//     const stopButton = document.querySelector(".stop");
//     const continueButton = document.querySelector(".continue");
  
//     let currentIndex = 0;
//     let intervalId;
  
//     function showImage(index) {
//       images.forEach((img, i) => {
//         img.style.display = i === index ? "block" : "none";
//       });
//     }
  
//     function startSlideshow() {
//       intervalId = setInterval(() => {
//         currentIndex = (currentIndex + 1) % images.length;
//         showImage(currentIndex);
//       }, 3000);
//     }
  
//     function stopSlideshow() {
//       clearInterval(intervalId);
//     }
  
//     stopButton.addEventListener("click", () => {
//       stopSlideshow();
//     });
  
//     continueButton.addEventListener("click", () => {
//       startSlideshow();
//     });
  
//     showImage(currentIndex);
//     startSlideshow();
//   });
   





