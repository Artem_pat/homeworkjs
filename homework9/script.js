const arr1 = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const arr2 = ["1", "2", "3", "sea", "user", 23];
console.dir(arr1)
function createList(arr, elem = document.body) {

    let ul = document.createElement('ul');
    
    elem.append(ul);
    
    for (let i = 0; i < arr.length; i++) {
        let li = document.createElement('li');
        li.textContent = arr[i];
        ul.append(li);
    }
    return ul;

}

createList(arr2);

