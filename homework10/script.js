
let tabList = document.querySelector('.tabs');
tabList.addEventListener('click', createTabs);

function createTabs(event) {

    let items = document.querySelectorAll('.tabs-title');
    console.log(items);

    for (let item of items) {
        item.classList.remove('active');
    }
    event.target.classList.add('active');
    console.log(event.target);

    let itemTexts = document.querySelectorAll('.tabs-content li');
    console.log(itemTexts);

    for (let itemText of itemTexts) {
        if (itemText.dataset.text !== event.target.dataset.name) {
            itemText.hidden = true;
        } else {
            itemText.hidden = false;
        }

    }
}