// Кнопка для переключення теми на JORNEY

// const btnChange = document.querySelector('.header-title');
// const form = document.querySelector('.form');
// const formInputs = form.querySelectorAll('.form-item, .form-name, .custom-checkbox');
// const checkBox = document.querySelector('.checkbox-block');
// const img = document.querySelector('.container')

// document.addEventListener('DOMContentLoaded', getClas);

// btnChange.addEventListener('click', () => {

//     console.log(form.classList);

//     form.classList.toggle('dark');
//     img.style.filter = 'brightness(0.5)';
//     localStorage.setItem('braitness', 'brightness(0.5)');
//     formInputs.forEach(el => {
//         el.classList.toggle('dark');
//     })
//     checkBox.classList.toggle('dark-letters');
//     if (form.classList.contains('dark')) {
//         localStorage.setItem('clas', 'dark');
//     } else {
//         localStorage.removeItem('clas', 'dark');
//     }
// });

// function getClas() {
//     let braitness = localStorage('braitness')
//     let clas = localStorage.getItem('clas');
//     console.log(clas);
//     if (!clas || !braitness) {
//         return;
//     }
//     img.style.filter = 'brightness(0.5)';
//     checkBox.classList.add('dark-letters')
//     form.classList.add(clas);
//     formInputs.forEach(el => {
//         el.classList.add(clas);
//     })

// }

const themeHead = document.querySelector('head');
//const btnChange = document.querySelector('.header-title');

const ourLink = themeHead.querySelector('[title = "theme"]');

let dayBtn = document.querySelector('.day');
let nightBtn = document.querySelector('.night');


nightBtn.addEventListener('click', () => {
    ourLink.setAttribute('href', './css/dark.css');
    nightBtn.style.display = 'none';
    dayBtn.style.display = 'block';
    localStorage.setItem('theme', './css/dark.css');
});

dayBtn.addEventListener('click', () => {
    ourLink.setAttribute('href', './css/light.css');
    dayBtn.style.display = 'none';
    nightBtn.style.display = 'block';
    localStorage.setItem('theme', './css/light.css');
});

document.addEventListener('DOMContentLoaded', () => {
    let nowTheme = localStorage.getItem('theme');
    console.log(nowTheme);
    if (!nowTheme) {
        return;
    }
    ourLink.setAttribute('href', nowTheme);
})










