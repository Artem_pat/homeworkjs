//1) DOM - document object model, кожний тег є об'єктом html, в кожног тега є свої "діти" об'єктиб до яких можна звернутися, і записати у змінну.
//   Dom складає дерево дерево тегів.
//2) B innerText ми можемо писати лише текст який буде вкладений в один тег, в innerHTML ми можемо вписати крім тексту ще вкладені теги.
//3) Ми можемо звернутися за допомогою методів getElementBy* або querySelector, querySelectorAll. querySelector повертає статичну колекцію,
//   getElementBy* повертає живу коллекцію. Найчастіше використовують querySelector

let par = document.querySelectorAll('p');
console.log(par);

for (let i = 0; i < par.length; i++) {
    
    par[i].style.color = "#ff0000";

}

let elemId = document.getElementById('optionsList');
console.log(elemId); 

let parentElemId = elemId.parentNode;
console.log(parentElemId);

let childrenElenId = elemId.childNodes;
console.log(childrenElenId);

for (let i = 0; i < childrenElenId.length; i++) {
    
    console.log(childrenElenId[i].nodeName, childrenElenId[i].nodeType);
    
}

let parag = document.querySelector('#testParagraph');
console.log(parag);
parag.innerText = "This is a paragraph";

//let mainHeaderElems = document.getElementsByClassName('main-header')[0];
let mainHeaderElems = document.querySelector('.main-header');

console.log(mainHeaderElems);

for (let i = 0; i < mainHeaderElems.children.length; i++) {
    mainHeaderElems.children[i].classList.add('nav-item');    
}
console.log(mainHeaderElems.children);

let sectionTitleElems = document.querySelectorAll('.section-title');
console.log(sectionTitleElems);

for (let i = 0; i < sectionTitleElems.length; i++) {
    sectionTitleElems[i].classList.remove('section-title');
}

console.log(sectionTitleElems);
